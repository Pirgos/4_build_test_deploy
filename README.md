# 4_build_test_deploy

build -> test -> deploy pipeline - as a standard software delivery pipeline

# build
* source: [main:tex](https://gitlab.com/-/ide/project/Pirgos/4_build_test_deploy/tree/main/-/main.tex/)
* build as transform main.tex to index.html

# test

* simple check of content base on grep

# deploy 

* our productional [www](https://pirgos.gitlab.io/4_build_test_deploy) 
* taging as as trigger for deployment

  
# TODO
* enrich project - add user email, who did last commit...

HINT: https://docs.gitlab.com/ee/ci/variables/
